import { request,qiniuUploaderFun,filtureDate } from "../../../request/request"
import { showToast } from "../../../libs/asyncWx.js"
Page({
  data: {
    id: "",
    isShowTrue:false,
    CargoFileNameZC:"",
    selectType:"image",
    driverInfo:{},
    checkFileNamw:"",
  },

    // 去查看路线
  goMap(e) {
      let id = e.currentTarget.dataset.id
      wx.navigateTo({
        url: `../map/map?id=${id}`
      })
    },
    // 查看凭证
    showFileInfo(e){
      let src = e.currentTarget.dataset.src
      if(src && src.length > 0){
        console.log("aaaa")
        wx.previewMedia({
          sources:[
            {
              url:src, // 当前显示图片/视频的http链接
            }
          ],
          url:src
        })
      }
    },
  onLoad: function (options) {
    this.setData({
      id:options.id
    })
  },
  getDetails(id){
    let data = {
      id:id
    }
    request(`/api/transport-order/${id}`, data,false,"GET").then((res) => {
      if(res.data.code == 200){
        let driverInfo = res.data.data
        driverInfo.transport_at = driverInfo.transport_at? filtureDate(driverInfo.transport_at ):"--"
        driverInfo.finish_at = driverInfo.finish_at?filtureDate(driverInfo.finish_at ):"--"
        this.setData({
          driverInfo:driverInfo
        })
      }
    })
  },
  onReady: function () {
    wx.setNavigationBarColor({
      frontColor: '#ffffff',
      backgroundColor: '#0C89FB',
    })
  },

  // 抵达装车
  arriveClick(e){
    let that = this   
    let status = e.currentTarget.dataset.status
    let id = e.currentTarget.dataset.id
    let data = {
      transport_order_id :id,
      next_transport_status:status
    }
    this.setData({
      transport_order_id:id,
      next_transport_status:status
    })
    if (status == 1) {  // 赶往装货中  操作为已达装货   status+1
      wx.showModal({
        title: "确定抵达装车点",
        content: '您确定抵达装车地点吗？',
        success(res) {
          if (res.confirm) {
            data.next_transport_status = Number(status) + 1
            that.changeStatus(data)
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
    } else if (status == 2) {
      data.next_transport_status = Number(status) + 1
      wx.showModal({
        title: "确定装车",
        content: '您确定开始装车吗？',
        success(res) {
          if (res.confirm) {
            that.changeStatus(data)
          } else if (res.cancel) {
          }
        }
      })
    } else if (status == 3 || status == 5) {
      that.setData({
        isShowTrue:true,
        CargoFileNameZC:null,
        selectType:null,
        checkFileNamw:""
      })
      wx.getLocation({
        type: 'wgs84',
        success(res) {
          that.setData({            
            lat: res.latitude,
            lng: res.longitude
          })
        }
      })  
      data.next_transport_status = Number(status) + 1
    } else if (status == 4) {
      data.next_transport_status = Number(status) + 1
      wx.showModal({
        title: "确定抵达卸车",
        content: '您确定抵达卸车吗？',
        success(res) {
          if (res.confirm) {
            that.changeStatus(data)
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
    } else if (status == 6) {
      let src = e.currentTarget.dataset.src
      if(src && src.length > 0){
        wx.previewMedia({
          sources:[
            {
              url:src, // 当前显示图片/视频的http链接
            }
          ],
          url:src
        })
      }
    }
    
  },
  changeStatus(data){
    request('/api/transport-order/update-status', data,true,"POST").then((res) => {
      if(res.data.code == 200){
        showToast({
          title:"操作成功",
          icon:"success"
        })
        this.setData({
          isShowTrue:false,
          CargoFileNameZC:null,
        })
        this.getDetails(this.data.id)
      }else{
        showToast({
          title:res.data.msg,
          icon:"error"
        })
      }
    })
  },
  // 取消装车完成
  loadingCancel(){
    this.setData({
      isShowTrue:false,
      CargoFileNameZC:null,
      selectType:null
    })
  },
  // 装车完成 - 上传装车凭证
  uploadVideoZC(){
    let that = this;
    wx.chooseMedia({
      count:1,
      mediaType:["video","image"],
      sourceType: ['album', 'camera'],
      maxDuration: 30,
      success(res) {
        that.setData({
          selectType:res.type,
          CargoFileNameZC:res.tempFiles[0].tempFilePath,
          checkFileNamw:res.tempFiles[0].tempFilePath,
        })
      }
    })
  },
  // 确定上传
  cargoSaveZC() {
    let that = this
    if(this.data.CargoFileNameZC){
      request('/api/qiniu/uptoken', {},false,"GET").then((res) => {
        if(res.data.code == 200){
          this.setData({
            uptoken:res.data.data.uptoken
          })
          qiniuUploaderFun(this.data.CargoFileNameZC, this.data.selectType, this.data.uptoken).then((resp) => {
            let data = {
              transport_order_id :that.data.transport_order_id,
              next_transport_status :Number(that.data.next_transport_status)+1,
              thumb:resp.key,
              lat:that.data.lat,
              lng:that.data.lng,
            }
            that.changeStatus(data)
          })
        }
      }) 
    }else{
      showToast({
        title:"请选择文件上传"
      })
    }
      
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getDetails(this.data.id)
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})