import { request,qiniuUploaderFun,filtureDate } from "../../../request/request"
import { showToast } from "../../../libs/asyncWx.js"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isRuleTrue:false,
    isShowTrue:false,
    CargoFileNameZC:"",
    checkFileNamw:"",
    jie_statusList:[{name:"全部",id:0},{name:"待支付",id:1},{name:"已支付",id:2}],
    selectJie:0,
    orderStatusList:[
      {name:"全部",id:0},{name:"赶往装货",id:1},{name:"已达装货",id:2},
      {name:"装车中",id:3},{name:"运输中途",id:4},{name:"已达卸货",id:5},
      {name:"运输完毕",id:6},{name:"已接收",id:7},{name:"已取消",id:8},
    ],
    selectOrder:0,
    chan_order_no:"",
    selectDate:"",
    date_type :2,
    page:1,
    hasNext:false,
    list:[],
    chanfeiInfo:{},
    lat:"",
    lng:"",
    selectType:"image"

  },
  onLoad: function (options) {
    let year  = new Date().getFullYear();
    let month = new Date().getMonth() + 1;
    if(month < 10){
       month = "0"+month
    }
    this.setData({
      selectDate:year+'-'+month
    })
    
  },
  getList(isFirst = true){
    console.log(isFirst)
    let data = {
      date_type :2,
      transport_order_no:this.data.chan_order_no,
      month:this.data.selectDate,
      jiekuan_status  :this.data.selectJie,
      transport_status:this.data.selectOrder,
      page:this.data.page,
      limit:10
    }    
    request('/api/transport-order', data, false, "GET").then((res) => {
      let chanfeiInfo = res.data.data
      for (let val of chanfeiInfo.list) {
        val.chan_waste_info.zhuangche_at = filtureDate(val.chan_waste_info.zhuangche_at )
      }
      this.setData({
        chanfeiInfo:chanfeiInfo
      })
      if(isFirst){  // 是否第一次加载
        if(chanfeiInfo.list.length == data.limit){
          this.setData({
            hasNext:true
          })
        }
        this.setData({
          list:chanfeiInfo.list
        })
      }else{
        if(chanfeiInfo.list.length < data.limit){
          this.setData({
            hasNext:false
          })
        }
        this.setData({
          list:[...this.data.list,...chanfeiInfo.list]
        })
        
      }
    })
  },
  // 获取订单号
  getNum(e){
    this.setData({
      chan_order_no:e.detail.value,
      page:1
    })
    this.getList()
  },
  bindDateChange(val){
    this.setData({
      selectDate:val.detail.value,
      page:1
    })
    this.getList()
  },
  // 抵达装车
  arriveClick(e){
    let that = this   
    let status = e.currentTarget.dataset.status
    let id = e.currentTarget.dataset.id
    let data = {
      transport_order_id :id,
      next_transport_status:status
    }
    this.setData({
      transport_order_id:id,
      next_transport_status:status
    })
    if (status == 1) {  // 赶往装货中  操作为已达装货   status+1
      wx.showModal({
        title: "确定抵达装车点",
        content: '您确定抵达装车地点吗？',
        success(res) {
          if (res.confirm) {
            data.next_transport_status = Number(status) + 1
            that.changeStatus(data)
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
    } else if (status == 2) {
      data.next_transport_status = Number(status) + 1
      wx.showModal({
        title: "确定装车",
        content: '您确定开始装车吗？',
        success(res) {
          if (res.confirm) {
            that.changeStatus(data)
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
    } else if (status == 3 || status == 5) {    
      that.setData({
        isShowTrue:true,
        CargoFileNameZC:null,
        selectType:null,
        checkFileNamw:"",
      })
      wx.getLocation({
        type: 'wgs84',
        success(res) {
          that.setData({            
            lat: res.latitude,
            lng: res.longitude
          })
        }
      })    
      data.next_transport_status = Number(status) + 1  
    } else if (status == 4) {
      data.next_transport_status = Number(status) + 1
      wx.showModal({
        title: "确定抵达卸车",
        content: '您确定抵达卸车吗？',
        success(res) {
          if (res.confirm) {
            that.changeStatus(data)
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
    } else if (status == 6) {
      let src = e.currentTarget.dataset.src
      if(src && src.length > 0){
        wx.previewMedia({
          sources:[
            {
              url:src, // 当前显示图片/视频的http链接
            }
          ],
          url:src
        })        
      }
    }
    
  },
  changeStatus(data){
    request('/api/transport-order/update-status', data,true,"POST").then((res) => {
      if(res.data.code == 200){
        showToast({
          title:"操作成功",
          icon:"success"
        })
        this.setData({
          page:1,
          isShowTrue:false,
          CargoFileNameZC:null,
        })
        this.getList()
      }else{
        showToast({
          title:res.data.msg,
          icon:"error"
        })
      }
    })
  },
  // 取消装车完成
  loadingCancel(){
    this.setData({
      isShowTrue:false,
      CargoFileNameZC:null,
      selectType:null
    })
  },
  // 装车完成 - 上传装车凭证
  uploadVideoZC(){
    let that = this;
    wx.chooseMedia({
      count:1,
      mediaType:["video","image"],
      sourceType: ['album', 'camera'],
      maxDuration: 30,
      success(res) {
        that.setData({
          selectType:res.type,
          CargoFileNameZC:res.tempFiles[0].tempFilePath,
          checkFileNamw:res.tempFiles[0].tempFilePath,
        })
      }
    })
  },
  // 确定上传
  cargoSaveZC() {
    let that = this
    if(this.data.CargoFileNameZC){
      request('/api/qiniu/uptoken', {},false,"GET").then((res) => {
        if(res.data.code == 200){
          this.setData({
            uptoken:res.data.data.uptoken
          })
          qiniuUploaderFun(this.data.CargoFileNameZC, this.data.selectType, this.data.uptoken).then((resp) => {
            let data = {
              transport_order_id :that.data.transport_order_id,
              next_transport_status :Number(that.data.next_transport_status)+1,
              thumb:resp.key,
              lat:that.data.lat,
              lng:that.data.lng,
            }
            that.changeStatus(data)
          })
        }
      }) 
    }else{
      showToast({
        title:"请选择文件上传"
      })
    }
      
  },
  // 打开筛选弹窗
  showModel(){
    this.setData({
      isRuleTrue:true
    })
    // this.animation.translate(0, -358).step()
    // this.setData({ animation: this.animation.export() })
  },
  // 关闭筛选弹窗
  closeToast(e) {
    this.setData({
        isRuleTrue: false,
    })
    // this.animation.translate(0, 0).step()
    // this.setData({ animation: this.animation.export() })
  },
   // 选择结款状态
   changeJieStatus(e){
    this.setData({
      selectJie:e.currentTarget.dataset.id
    })
  },
   // 选择订单状态
   changeOrderStatus(e){
    this.setData({
      selectOrder:e.currentTarget.dataset.id
    })
  },
  bindTrue(){
    this.setData({
      isRuleTrue: false,
      page:1
    })
    this.getList()
  },
  // 去订单详情
  goDetails(e){
    let id = e.currentTarget.dataset.id
    wx.navigateTo({
      url: `../../../pages/driver/details/details?id=${id}`

    })
  },
  // 回到个人中心
  goCenter(){
    wx.navigateBack({
      delta: 1
    })
    // wx.navigateTo({
    //   url:"../../center/index"
    // })
  },

  

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.animation = wx.createAnimation();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getList(true)
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.setData({
        page:1
    })
    this.getList(true)
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if(this.data.hasNext){
      this.setData({
        page:this.data.page+1
      })
      this.getList(false)
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})