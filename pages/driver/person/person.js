// pages/driver/person/person.js
import { request,filtureDate } from "../../../request/request"
import { showToast } from "../../../libs/asyncWx.js"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo:{},
  },  
  // 预览图片
  previewImg(e){
    let src = e.currentTarget.dataset.src;
    let list = this.data.userInfo.idcard_thumb.map(item=>{
      return{
        url:item
      }
    })
    wx.previewMedia({
      sources:list,
      url:src
    })
  },
  onLoad: function (options) {
    request('/api/member', {},false,"GET").then((res) => {
      if (res.data.code == 200) {
        let userInfo = res.data.data
        userInfo.join_at = filtureDate(userInfo.join_at)
        userInfo.confirm_at = filtureDate(userInfo.confirm_at)
        this.setData({
          userInfo:userInfo
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})