import { request ,qiniuUploaderFun,filtureDate} from "../../../request/request"
import { showToast } from "../../../libs/asyncWx.js"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id:"",
    checkFileNamw:"",
    currentIndex:"",
    ysIndex:"",
    ysInfo: {},
    isShowCargo:false,
    CargoFileNameZC: "",
    uptoken: "",
    ysId:"",
  },
  // 去到路线
  lookLine(e) {
    let id = e.currentTarget.dataset.id
    wx.navigateTo({
      url: `../../../pages/chanfei/map/map?id=${id}`
    })
  },
  // 查看凭证
  lookIMG(){
    let src = this.data.ysInfo.chan_jieshou_thumb
    wx.previewMedia({
      sources:[
        {url:src}
      ],
      url:src
    })
  },
  onLoad: function (options) {
    this.setData({
      id:options.id,
      currentIndex:options.currentIndex,
      ysIndex:options.ysIndex
    })    
  },
  getDetais(id,currentIndex,ysIndex){
    let data = {
      id:id
    }
    request(`/api/chan-waste-order/${id}/data-list`,data,false,"GET").then((res) => {
      if (res.data.code == 200) {
        let ysInfo = res.data.data[currentIndex].transport_orders[ysIndex]
        ysInfo.transport_at = filtureDate(ysInfo.transport_at)
        ysInfo.finish_at = filtureDate(ysInfo.finish_at)
        this.setData({
          ysInfo:ysInfo
        })
      }
    })
  },
  // 上传凭证
  cargoEnd(e){
    this.setData({
      isShowCargo: true,
      ysId:e.currentTarget.dataset.id,
      checkFileNamw:""
    })
  },
  uploadVideoZC(){
    let that = this;
    wx.chooseMedia({
      count:1,
      mediaType:["video","image"],
      sourceType: ['album', 'camera'],
      maxDuration: 30,
      success(res) {
        that.setData({
          selectType:res.type,
          checkFileNamw : res.tempFiles[0].tempFilePath,
          CargoFileNameZC:res.tempFiles[0].tempFilePath,
        })
      }
    })
  },
  // 确定上传
  cargoSaveZC() {
    let that = this
    if(this.data.CargoFileNameZC){
      request('/api/qiniu/uptoken', {},false,"GET").then((res) => {
        if(res.data.code == 200){
          this.setData({
            uptoken:res.data.data.uptoken
          })
          qiniuUploaderFun(this.data.CargoFileNameZC, this.data.selectType, this.data.uptoken).then((resp) => {
            let data = {
              transport_order_id  :that.data.ysInfo.id,
              chan_jieshou_thumb:resp.key,
            }
            that.changeStatus(data)
          })
        }
      }) 
    }else{
      showToast({
        title:"请选择文件上传"
      })
    }
      
  },
  changeStatus(data) {
    let that = this
    request(`/api/chan-waste-order/${data.transport_order_id}/chan_jieshou_thumb`, data, true, "POST").then((res) => {
      if (res.data.code == 200) {
        showToast({
          title: "操作成功",
          icon:"success"
        })
        this.setData({
          isShowCargo:false,
          CargoFileNameZC:null,
          selectType:null
        })
        setTimeout(() => {
          that.getDetais(that.data.id,that.data.currentIndex,that.data.ysIndex)
        },1500)
      }
     })
   },
   // 取消完成
  cargoCancel(){
    this.setData({
      isShowCargo:false,
      CargoFileNameZC:null,
      selectType:null
    })
  },
  onReady: function () {

  },

  onShow: function () {
    let that = this
    that.getDetais(that.data.id,that.data.currentIndex,that.data.ysIndex)
  },

  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})