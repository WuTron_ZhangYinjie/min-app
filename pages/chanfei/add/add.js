import { request } from "../../../request/request"
import { showToast } from "../../../libs/asyncWx.js"
Page({
  data: {
    array: [
      { name: "离心式", id: 1 },
      { name: "履带式", id: 2 },
      { name: "板框式", id: 3 },
      { name: "其他", id: 4 }
    ],
    customItem: '',
    total_weight: "",
    wuni_water_rate: "",
    tuoshui_type: "",
    tuoshui_typeName: "",
    yunshu_purpose: "",
    yunshu_purposeName: "",
    yunshu_purposeList: [
      { name: "自然干化", id: 1 },
      { name: "深度脱水", id: 2 },
      { name: "土地利用", id: 3 },
      { name: "堆肥", id: 4 },
      { name: "卫生填埋", id: 5 },
      { name: "干化焚烧", id: 6 },
      { name: "混合焚烧", id: 7 },
      { name: "综合利用", id: 8 },
      { name: "其他", id: 9 },
    ],
    car_type: "",
    car_typeName: "",
    carList: [],
    carLongList: [],
    carLong_id: "",
    carLong_name: "",
    car_num: "",
    contact_name: "",
    contact_phone: "",
    region: [],
    regionName: "",
    province: "",
    city: "",
    area: "",
    address: "",
    zhuangche_at: "",


  },
  onLoad: function (options) {
    this.getCarType();
    this.getCarLong();
    this.getMemberInfo()
  },
  getMemberInfo() {
    request('/api/member', {}, false, "GET").then((res) => {
      if (res.data.code == 200) {
        let chanfeiInfo = res.data.data
        this.setData({
          contact_name: chanfeiInfo.person_charge,
          contact_phone: chanfeiInfo.phone,
          address: chanfeiInfo.address,
          regionName: chanfeiInfo.province + "/" + chanfeiInfo.city + "/" + chanfeiInfo.area,
          province: chanfeiInfo.province,
          city: chanfeiInfo.city,
          area: chanfeiInfo.area,
        })
      }
    })
  },
  // 获取车型list
  getCarType() {
    request('/api/transport/car-type', {}, false, "GET").then((res) => {
      if (res.data.code == 200) {
        let list = []
        for (let val in res.data.data) {
          let opt = {
            name: res.data.data[val],
            id: Number(val)
          }
          list.push(opt)
        }
        this.setData({
          carList: list
        })
      }
    })
  },
  // 获取车长list
  getCarLong() {
    request('/api/transport/car-long', {}, false, "GET").then((res) => {
      if (res.data.code == 200) {
        let list = []
        for (let val in res.data.data) {
          let opt = {
            name: res.data.data[val] + "  米",
            id: Number(val)
          }
          list.push(opt)
        }
        this.setData({
          carLongList: list
        })
      }
    })
  },

  bindPickerCarLong(e) {
    this.setData({
      carLong_id: this.data.carLongList[e.detail.value].id,
      carLong_name: this.data.carLongList[e.detail.value].name
    })
  },
  // 获取重量
  getWeight(e) {
    this.setData({
      total_weight: e.detail.value
    })
  },
  // 获取含水率
  getWater(e) {
    this.setData({
      wuni_water_rate: e.detail.value
    })
  },
  // 获取脱水方式
  bindPickerWater(e) {
    this.setData({
      tuoshui_type: this.data.array[e.detail.value].id,
      tuoshui_typeName: this.data.array[e.detail.value].name
    })
  },
  // 外运目的
  bindPickerYunshu(e) {
    this.setData({
      yunshu_purpose: this.data.yunshu_purposeList[e.detail.value].id,
      yunshu_purposeName: this.data.yunshu_purposeList[e.detail.value].name
    })
  },
  // 车辆类型
  bindPickerCar(e) {
    this.setData({
      car_type: this.data.carList[e.detail.value].id,
      car_typeName: this.data.carList[e.detail.value].name
    })
  },
  // 车辆数量
  getCarNum(e) {
    this.setData({
      car_num: e.detail.value
    })
  },
  //获取联系人
  getName(e) {
    this.setData({
      contact_name: e.detail.value
    })
  },
  // 获取手机号
  getPhone(e) {
    this.setData({
      contact_phone: e.detail.value
    })
  },
  // 获取省市区
  bindRegionChange(e) {
    this.setData({
      regionName: e.detail.value[0] + '/' + e.detail.value[1] + '/' + e.detail.value[2],
      province: e.detail.value[0],
      city: e.detail.value[1],
      area: e.detail.value[2],
    })
  },

  // 详细地址
  getAddress(e) {
    this.setData({
      address: e.detail.value
    })
  },
  // 装车时间
  bindPickerDate(e) {
    this.setData({
      zhuangche_at: e.detail.value
    })
  },
  // 提交
  submitForm() {
    let data = {
      total_weight: this.data.total_weight,
      wuni_water_rate: this.data.wuni_water_rate,
      tuoshui_type: this.data.tuoshui_type,
      yunshu_purpose: this.data.yunshu_purpose,
      car_type: this.data.car_type,
      car_long: this.data.carLong_id,
      car_num: this.data.car_num,
      contact_name: this.data.contact_name,
      contact_phone: this.data.contact_phone,
      province: this.data.province,
      city: this.data.city,
      area: this.data.area,
      address: this.data.address,
      zhuangche_at: this.data.zhuangche_at,
    }
    request('/api/chan-waste-order', data, true, "POST").then((res) => {
      console.log(res.data.code)
      if (res.data.code == 200) {
        showToast({
          title: res.data.msg,
          icon: "success"
        })
        setTimeout(() => {
          wx.navigateTo({
            url: '../../../pages/chanfei/order/order'
          })
        }, 1500)

      } else {
        showToast({
          title: res.data.msg,
          icon: "error"
        })
      }
    })
  },

  onReady: function () {

  },

  onShow: function () {

  },
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})