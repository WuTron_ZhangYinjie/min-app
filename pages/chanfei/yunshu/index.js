
import { request } from "../../../request/request"
import { showToast } from "../../../libs/asyncWx.js"
Page({

  data: {
    id:null,
    currentIndex:0,
    yunshuList:[],
  },
  // 去到运输方详情
  goysDetails(e){
    let that = this
    let ysIndex = e.currentTarget.dataset.index
    wx.navigateTo({
      url: `../../../pages/chanfei/ysDetails/ysDetails?id=${that.data.id}&currentIndex=${that.data.currentIndex}&ysIndex=${ysIndex}`
    })
  },
  onLoad: function (options) {
    this.setData({
      id:options.id,
      currentIndex:options.index
    })
    this.getDetais(options.id)
  },
    // 获取产废信息
  getDetais(id){
      let data = {
        id:id
      }
      request(`/api/chan-waste-order/${id}/data-list`,data,false,"GET").then((res) => {
        if(res.data.code == 200){
          this.setData({
            yunshuList:res.data.data[this.data.currentIndex].transport_orders
          })
        }
      })
  },

  onReady: function () {

  },
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})