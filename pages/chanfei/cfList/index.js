import { request } from "../../../request/request"
import { showToast } from "../../../libs/asyncWx.js"
let app =  getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    chufList: [],
    hasNext:false,
    baseUrl:app.globalData.xhrUrl,
    page:1,
  },  
  // 预览图片
  previewImg(e){
    let src = e.currentTarget.dataset.src;
    let list = this.data.chufList[e.currentTarget.dataset.index].file_url.map((item) =>{
      return {
        url:item
      }
    })
    wx.previewMedia({
      sources:list,
      url:src
    })
  },
  onLoad: function (options) {

  },
  getList(isFirst) {
    let data = {
      page:this.data.page,
      limit:10
    }
    request('/api/chan-waste/chu-waste-list', data,false,"GET").then((res) => {
      if(isFirst){  // 是否第一次加载
        if(res.data.data.list.length == data.limit){
          this.setData({
            hasNext:true
          })
        }
        this.setData({
          chufList:res.data.data.list
        })
      }else{
        if(res.data.data.list.length < data.limit){
          this.setData({
            hasNext:false
          })
        }
        this.setData({
          chufList:[...this.data.chufList,...res.data.data.list]
        })
        
      }
    })
  },

  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getList(true)
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
     this.setData({
        page:1
      })
      this.getList(true)
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if(this.data.hasNext){
      this.setData({
        page:this.data.page+1
      })
      this.getList(false)
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})