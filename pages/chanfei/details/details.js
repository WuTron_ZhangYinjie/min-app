
import { request ,filtureDate} from "../../../request/request"
import { showToast } from "../../../libs/asyncWx.js"
Page({
  data: {
    chanfInfo:{},
    chanfName:"",
    chufeiList:[],
    id:null,
  },
  // 查看凭证
  showFileInfo(e){
    let src = e.currentTarget.dataset.src
    if(src && src.length > 0){
      wx.previewMedia({
        sources:[{url:src}],
        url:src
      })
    }
  },
    // 去运输方明细列表
    goYSlist(e){
      let that = this;
      let index = e.currentTarget.dataset.index
      if(this.data.chufeiList[index].transport_orders.length > 0){
        wx.navigateTo({
          url: `../../../pages/chanfei/yunshu/index?index=${index}&id=${that.data.id}`
        })
      }else{
        showToast({
          title:"暂无运输信息",
          icon:"error"
        })
      }
      
    },

  /**
   * 生命周期函数--监听页面加载
   */

  onLoad: function (options) {
    let chanfName = JSON.parse(wx.getStorageSync('userInfo')).name    
    this.setData({
      chanfName:chanfName,
      id:options.id
    })
    this.getDetais(options.id)
    this.getChufeiDetails(options.id)
  
  },
  // 获取产废信息
  getDetais(id){
    let data = {
      id:id
    }
    request(`/api/chan-waste-order/${id}`,data,false,"GET").then((res) => {
      if (res.data.code == 200) {
        let chanfeiInfo = res.data.data

        chanfeiInfo.zhuangche_at = filtureDate(chanfeiInfo.zhuangche_at )

        this.setData({
          chanfInfo:chanfeiInfo
        })
      }
    })
  },
  // 获取处废和运输方信息
  getChufeiDetails(id){
    let data = {
      id:id
    }
    request(`/api/chan-waste-order/${id}/data-list`,data,false,"GET").then((res) => {
      if(res.data.code == 200){
        this.setData({
          chufeiList:res.data.data
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    wx.setNavigationBarColor({
      frontColor: '#ffffff',
      backgroundColor: '#0C89FB',
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})