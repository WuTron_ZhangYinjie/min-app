import { request,filtureDate } from "../../../request/request"
import { showToast } from "../../../libs/asyncWx.js"
var app = getApp()
Page({
  data: {
    userInfo:{},
    chufeiStr:"",
    imgList: []
  },  
  // 预览图片
  previewImg(e){
    let src = e.currentTarget.dataset.src;
    wx.previewMedia({
      sources:this.data.imgList,
      url:src
    })
  },
  gocfList(){
    wx.navigateTo({
      url:"../../../pages/chanfei/cfList/index"
    })
  },
  onLoad: function (options) {
    request('/api/member', {},false,"GET").then((res) => {
      if(res.data.code == 200){
        let userInfo = res.data.data
        let chuList = res.data.data.chu_waste_list.map((item) =>{
          return item.name
        })
        let imgList = res.data.data.file_url.map((item) =>{
          return {
            url:item
          }
        })
        userInfo.created_at = filtureDate(userInfo.created_at)
        userInfo.confirm_at = filtureDate(userInfo.confirm_at)
        this.setData({
          userInfo:userInfo,
          imgList:imgList,
          chufeiStr:chuList.join(',')
        })
      }
    })
  },

  onReady: function () {

  },
  onShow: function () {

  },

  onHide: function () {

  },
  onUnload: function () {

  },
  onPullDownRefresh: function () {

  },
  onReachBottom: function () {

  },
  onShareAppMessage: function () {

  }
})