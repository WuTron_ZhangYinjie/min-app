import { request,filtureDate } from "../../../request/request"
import { showToast } from "../../../libs/asyncWx.js"
Page({
  data: {
    isRuleTrue:false,
    isShowTrue:false,
    isShowCargo:false,
    jie_statusList:[{name:"全部",id:0},{name:"待支付",id:1},{name:"已支付",id:2}],
    selectJie:0,
    orderStatusList:[
      {name:"全部",id:0},{name:"新订单",id:1},{name:"待派车",id:2},
      {name:"待装车",id:3},{name:"部分装车",id:4},{name:"运输中",id:5},
      {name:"部分完成",id:6},{name:"已完成",id:7},
    ],
    selectOrder:0,
    chan_order_no:"",
    selectDate:"",
    date_type :2,
    page:1,
    hasNext:false,
    list:[],
    chanfeiInfo:{},
  },
  bindDateChange(val){
    this.setData({
      selectDate:val.detail.value,
      page:1
    })
    this.getList()
  },
  // 获取订单号
  getNum(e){
    this.setData({
      chan_order_no:e.detail.value,
      page:1
    })
    this.getList()
  },
  // 打开筛选弹窗
  showModel(){
    this.setData({
      isRuleTrue:true
    })
  },
  // 关闭筛选弹窗
  closeToast(e) {
    this.setData({
        isRuleTrue: false,
    })
  },
  // 确定筛选
  filtureList(){    
    this.setData({
        isRuleTrue: false,
        page:1
    })
    this.getList()
  },
  // 选择结款状态
  changeJieStatus(e){
    this.setData({
      selectJie:e.currentTarget.dataset.id
    })
  },
  // 选择订单状态
  changeOrderStatus(e){
    this.setData({
      selectOrder:e.currentTarget.dataset.id
    })
  },
  // 去订单详情
  goDetails(e){
    wx.navigateTo({
      url: `../../../pages/chanfei/details/details?id=${e.currentTarget.dataset.id}`
    })
  },
  // 回到个人中心
  goCenter(){
    wx.navigateBack({
      delta: 1
    })
  },
  // 新增订单
  addClick(){
    wx.navigateTo({
      url: '../../../pages/chanfei/add/add'
    })
  },
  onLoad: function (options) {
    let year  = new Date().getFullYear();
    let month = new Date().getMonth() + 1;
    if(month < 10){
       month = "0"+month
    }
    this.setData({
      selectDate:year+'-'+month
    })
    this.getList()
    
  },
   // 获取订单列表
  getList(isFirst = true){
    console.log(isFirst)
    let data = {
      date_type :2,
      chan_order_no:this.data.chan_order_no,
      month:this.data.selectDate,
      chanfei_jiekuan_status  :this.data.selectJie,
      status:this.data.selectOrder,
      page:this.data.page,
      limit:10
    }    
    request('/api/chan-waste-order', data,false,"GET").then((res) => {

      let chanfeiInfo = res.data.data
      for (let val of chanfeiInfo.list) {
        val.zhuangche_at = filtureDate(val.zhuangche_at )
      }
      this.setData({
        chanfeiInfo:chanfeiInfo
      })
      if(isFirst){  // 是否第一次加载
        if(chanfeiInfo.length == data.limit){
          this.setData({
            hasNext:true
          })
        }
        this.setData({
          list:chanfeiInfo.list
        })
      }else{
        if(chanfeiInfo.list.length < data.limit){
          this.setData({
            hasNext:false
          })
        }
        this.setData({
          list:[...this.data.list,...chanfeiInfo.list]
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.animation = wx.createAnimation();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if(this.data.hasNext){
      this.setData({
        page:this.data.page+1
      })
      this.getList(false)
      console.log("page===="+this.data.page)
    }
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})