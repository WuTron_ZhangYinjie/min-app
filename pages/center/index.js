const app = getApp()
const qqMap = require('../../libs/qqmap-wx-jssdk')
import { request, filtureDate } from "../../request/request.js"
import { showToast } from "../../libs/asyncWx.js"
const QQMapWX = new qqMap({
  key: app.globalData.key // 必填
});
Page({
  data: {
    token: null,
    loginTppe: null,
    userInfo: {},
    isLook: true,
  },
  onLoad: function (options) {
    let loginTppe = wx.getStorageSync('loginTppe')
    let token = wx.getStorageSync('token')
    if (token && loginTppe) {
      this.setData({
        isLook: false
      })
    }
  },
  // 退出登录
  loginOut() {
    wx.showModal({
      title: "退出登录",
      content: '您确定退出登录吗？',
      success(res) {
        if (res.confirm) {
          wx.navigateTo({
            url: '../../pages/login/index'
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  getUserInfo() {
    request('/api/member', {}, false, "GET").then((res) => {
      if (res.data.code == 200) {
        let chanfeiInfo = res.data.data
        chanfeiInfo.join_at = filtureDate(chanfeiInfo.join_at)
        chanfeiInfo.created_at = filtureDate(chanfeiInfo.created_at)
        this.setData({
          userInfo: chanfeiInfo
        })
      } else if (res.data.code == 100) {
        wx.navigateTo({
          url: '../../pages/login/index'
        })
      }
    })
  },
  // 导航跳转
  goOrder() {
    let loginTppe = wx.getStorageSync('loginTppe')
    let token = wx.getStorageSync('token')
    if (!loginTppe || !token) {
      wx.reLaunch({
        url: '../../pages/login/index'
      })
    } else {
      // 跳转其他
      wx.navigateTo({
        url: '../../pages/driver/order/order'
      })
    }
  },
  routerChange() {
    wx.reLaunch({
      url: '../../pages/login/index'
    })
  },
  // 刷新位置
  routerLocation() {
    let that = this;
    wx.getLocation({
      type: 'wgs84',
      isHighAccuracy: true,
      highAccuracyExpireTime: 6000,
      altitude: true,
      success(res) {
        const latitude = res.latitude
        const longitude = res.longitude
        that.getAddress(latitude, longitude)
      }
    })
  },
  getAddress(latitude, longitude) {
    let that = this;
    QQMapWX.reverseGeocoder({
      location: {
        latitude: latitude,
        longitude: longitude
      },
      success: function (res) {
        that.setAddress(res.result.address, latitude, longitude)
      },
      fail: function (res) {
        console.log(res);
      },
    })
  },

  setAddress(address, latitude, longitude) {
    let data = {
      newest_location: address,
      latitude: latitude,
      longitude: longitude
    }
    request('/api/member/newest-location', data, true, "POST").then((res) => {
      if (res.data.code == 200) {
        this.getUserInfo()
      } else if (res.data.code == 100) {
        wx.navigateTo({
          url: '../../pages/login/index'
        })
      }
    })
  },

  // 司机端个人信息
  lookInfomation() {
    wx.navigateTo({
      url: "../../pages/driver/person/person"
    })
  },
  // 司机端车辆信息
  lookCarInfomation() {
    wx.navigateTo({
      url: "../../pages/driver/car/car"
    })
  },
  // 去产废方个人信息
  lookInfomationchanfei() {
    wx.navigateTo({
      url: "../../pages/chanfei/person/person"
    })
  },
  // 去产废方订单
  goChanfeiOrder() {
    wx.navigateTo({
      url: "../../pages/chanfei/order/order"
    })
  },
  // 去处废方个人信息
  lookInfomationchufei() {
    wx.navigateTo({
      url: "../../pages/chufei/person/person"
    })
  },
  // 去处废订单
  goChufeiOrder() {
    wx.navigateTo({
      url: "../../pages/chufei/order/order"
    })
  },
  // 去修改密码
  editPassword(e) {
    let loginType = e.currentTarget.dataset.id
    wx.navigateTo({
      url: "../../pages/password/index?loginType=" + loginType
    })
  },
  // 打电话
  makePhone() {
    // wx.navigateTo({
    //   url:"../../pages/login/index"
    // })
    wx.makePhoneCall({
      phoneNumber: '1340000' //仅为示例，并非真实的电话号码
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let loginTppe = wx.getStorageSync('loginTppe')
    let token = wx.getStorageSync('token')
    if (token && loginTppe) {
      this.setData({
        token: token,
        loginTppe: loginTppe,
      })
      this.getUserInfo()
    }

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})