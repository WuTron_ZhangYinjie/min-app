import { request, $direction } from "../../../request/request"
Page({
  data: {
    latitude: '',
    longitude: '',
    polyline: [],
    markers: [],
  },
  async exportPoly1(coordinate, location) {
    //路线规划 
    return pl
  },
  async onLoad(options) {

    // let location =  this.$getLocation(true);
    this.setData({
      id: options.id,
      // latitude: location.latitude,
      // longitude: location.longitude
    })
    let data = {
      order_id: options.id
    }
    request(`/api/member/driving-route`, data, false, "GET").then((resp) => {
      if (resp.data.code == 200) {
        // console.log(resp)
        let result = resp.data.data
        let coordinate1 = {  // 司机位置
          lat: result.transport_lat,
          lng: result.transport_lng,
        }
        let coordinate2 = {   // 产废位置
          lat: result.chan_waste_lat,
          lng: result.chan_waste_lng,
        }
        let nextConr1 = {  // 处废位置
          lat: result.chu_waste_lat,
          lng: result.chu_waste_lng,
        }
        if (result.transport_status == 1) {  // 赶往装货状态  显示司机位置到产废方路线   产废方到处废方路线          

          let markers = [
            {   // 产废位置
              iconPath: "../../../images/base/start.png",
              id: 0,
              latitude: coordinate1.lat,
              longitude: coordinate1.lng,
              width: 36,
              height: 36,
            },
            {   //  司机位置
              iconPath: "../../../images/base/current.png",
              id: 1,
              latitude: coordinate2.lat,
              longitude: coordinate2.lng,
              width: 36,
              height: 36,
            },
            {     // 处废位置
              iconPath: "../../../images/base/end.png",
              id: 2,
              latitude: nextConr1.lat,
              longitude: nextConr1.lng,
              width: 36,
              height: 36,
            }
          ]
          let pl_1 = [];
          $direction({ latitude: coordinate1.lat, longitude: coordinate1.lng }, { latitude: coordinate2.lat, longitude: coordinate2.lng }, 1).then((res) => {
            let coors = res.polyline;
            let kr = 1000000;
            for (var i = 2; i < coors.length; i++) {
              coors[i] = Number(coors[i - 2]) + Number(coors[i]) / kr;
            }
            for (var i = 0; i < coors.length; i += 2) {
              pl_1.push({
                latitude: coors[i],
                longitude: coors[i + 1]
              })
            }
            let opt1 = {
              points: pl_1,
              color: "#1BA078",
              width: 6,
              arrowLine: true
            }
            this.setData({
              latitude: nextConr1.lat,
              longitude: nextConr1.lng,
              markers: markers,
              ["polyline[0]"]: opt1
            })
          })
          let pl_2 = [];
          $direction({ latitude: coordinate2.lat, longitude: coordinate2.lng }, { latitude: nextConr1.lat, longitude: nextConr1.lng }, 1).then((res) => {
            let coors = res.polyline;
            let kr = 1000000;
            for (var i = 2; i < coors.length; i++) {
              coors[i] = Number(coors[i - 2]) + Number(coors[i]) / kr;
            }
            for (var i = 0; i < coors.length; i += 2) {
              pl_2.push({
                latitude: coors[i],
                longitude: coors[i + 1]
              })
            }
            let opt2 = {
              points: pl_2,
              color: "#f20",
              width: 6,
              arrowLine: true
            }
            this.setData({
              latitude: nextConr1.lat,
              longitude: nextConr1.lng,
              markers: markers,
              ["polyline[1]"]: opt2
            })

          })
        } else if (result.transport_status == 2 || result.transport_status == 3 || result.transport_status == 6) { // 或者已达装货  装车中  产废方到处废方路线
          let markers = [
            {   // 产废位置
              iconPath: "../../../images/base/start.png",
              id: 0,
              latitude: coordinate1.lat,
              longitude: coordinate1.lng,
              width: 36,
              height: 36,
            },
            {   //  司机位置
              iconPath: "../../../images/base/current.png",
              id: 1,
              latitude: coordinate2.lat,
              longitude: coordinate2.lng,
              width: 36,
              height: 36,
            },
            {     // 处废位置
              iconPath: "../../../images/base/end.png",
              id: 2,
              latitude: nextConr1.lat,
              longitude: nextConr1.lng,
              width: 36,
              height: 36,
            }
          ]
          let pl_2 = [];
          $direction({ latitude: coordinate2.lat, longitude: coordinate2.lng }, { latitude: nextConr1.lat, longitude: nextConr1.lng }, 1).then((res) => {
            let coors = res.polyline;
            let kr = 1000000;
            for (var i = 2; i < coors.length; i++) {
              coors[i] = Number(coors[i - 2]) + Number(coors[i]) / kr;
            }
            for (var i = 0; i < coors.length; i += 2) {
              pl_2.push({
                latitude: coors[i],
                longitude: coors[i + 1]
              })
            }
            let opt2 = {
              points: pl_2,
              color: "#1BA078",
              width: 6,
              arrowLine: true
            }
            this.setData({
              latitude: nextConr1.lat,
              longitude: nextConr1.lng,
              markers: markers,
              ["polyline[0]"]: opt2
            })

          })
        } else if (result.transport_status == 7 || result.transport_status == 4 || result.transport_status == 5) {
          let markers = [
            {   // 产废位置
              iconPath: "../../../images/base/start.png",
              id: 0,
              latitude: coordinate1.lat,
              longitude: coordinate1.lng,
              width: 36,
              height: 36,
            },
            {   //  司机位置
              iconPath: "../../../images/base/current.png",
              id: 1,
              latitude: coordinate2.lat,
              longitude: coordinate2.lng,
              width: 36,
              height: 36,
            },
            {     // 处废位置
              iconPath: "../../../images/base/end.png",
              id: 2,
              latitude: nextConr1.lat,
              longitude: nextConr1.lng,
              width: 36,
              height: 36,
            }
          ]
          let pl_2 = [];
          $direction({ latitude: coordinate1.lat, longitude: coordinate1.lng }, { latitude: nextConr1.lat, longitude: nextConr1.lng }, 1).then((res) => {
            let coors = res.polyline;
            let kr = 1000000;
            for (var i = 2; i < coors.length; i++) {
              coors[i] = Number(coors[i - 2]) + Number(coors[i]) / kr;
            }
            for (var i = 0; i < coors.length; i += 2) {
              pl_2.push({
                latitude: coors[i],
                longitude: coors[i + 1]
              })
            }
            let opt2 = {
              points: pl_2,
              color: "#1BA078",
              width: 6,
              arrowLine: true
            }
            this.setData({
              latitude: nextConr1.lat,
              longitude: nextConr1.lng,
              markers: markers,
              ["polyline[0]"]: opt2
            })

          })
        }
      }

    })

  },

  /**
   * 获取用户当前定位(拉起授权)
   * @param {boolean} [isAgain=false] - 是否重新拉起授权
   * @return {Promise<object>} 返回对象
   */
  $getLocation(isAgain) {
    const that = this;
    wx.showLoading({
      title: '获取位置中....',
    })
    return new Promise((resolve, reject) => {
      wx.getLocation({
        type: 'gcj02',
        isHighAccuracy: true,
        highAccuracyExpireTime: 10000,
        success(res) {
          resolve({
            latitude: res.latitude,
            longitude: res.longitude
          })
        },
        complete() {
          wx.hideLoading()
        },
        async fail(e) {
          if (e.errMsg == "getLocation:fail auth deny") {
            isAgain && resolve(await that._getLocationFail())
          } else {
            reject(e)
          }
        }
      })
    })
  },

  onReady: function () {
    console.log('onReady')
  },
  onShow: function () {

  },
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})