// pages/driver/person/person.js
import { request, filtureDate } from "../../../request/request"
import { showToast } from "../../../libs/asyncWx.js"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {},
    imgList: []
  },
  // 预览图片
  previewImg(e) {
    let src = e.currentTarget.dataset.src;
    wx.previewMedia({
      sources:this.data.imgList,
      url:src
    })
  },
  onLoad: function (options) {
    request('/api/member', {}, false, "GET").then((res) => {
      if (res.data.code == 200) {
        let userInfo = res.data.data
        userInfo.created_at = filtureDate(userInfo.created_at)
        userInfo.confirm_at = filtureDate(userInfo.confirm_at)
        let list = res.data.data.file_url.map(item=>{
          return {
            url:item
          }
        })
        this.setData({
          userInfo: userInfo,
          imgList: list
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})