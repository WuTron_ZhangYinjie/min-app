import { request,qiniuUploaderFun,filtureDate } from "../../../request/request"
import { showToast } from "../../../libs/asyncWx.js"


Page({

  data: {
    orderId: "",
    orderStatus:0,
    isShowTrue:false,
    isShowCargo: false,
    driverInfo: null,
    reasonText: "",
    CargoFileNameZC: "",
    uptoken:"",
    checkFileNamw:"",
  },


  // 同意接单
  arriveClick() {
    let that = this
    wx.showModal({
      title:"同意接单",
      content: '您确定同意接单吗？',
      success (res) {
        if (res.confirm) {
          let data = {
            id :that.data.orderId 
          }
          request(`/api/chu-waste-order/${that.data.orderId }/approve`, data,true,"POST").then((res) => {
            if (res.data.code == 200) {
              showToast({
                title: "操作成功",
                icon:"success"
              })
              setTimeout(() => {
                that.getDetails(that.data.orderId)
               },1500)
            } else {
              showToast({
                title: res.data.msg,
                icon:"error"
              })
            }
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  // 拒绝接单
  arriveNotClick(){
    this.setData({
      isShowTrue:true
    })
  },
  // 获取拒绝内容
  getReason(e) {
    let text = e.detail.value
    this.setData({
      reasonText:text
    })
  },
  // 取消拒绝
  loadingCancel(){
    this.setData({
      isShowTrue:false
    })
  },
  // 提交拒绝
  loadingSubmit() {
    let that = this
    let data = {
      id: that.data.orderId,
      refuse_reason :that.data.reasonText
    }
    request(`/api/chu-waste-order/${that.data.orderId }/refuse`, data,true,"POST").then((res) => {
      if (res.data.code == 200) {
        showToast({
          title: "操作成功",
          icon:"success"
        })
        that.loadingCancel()
        setTimeout(() => {
          that.getDetails(that.data.orderId)
          },1500)
      } else {
        showToast({
          title: res.data.msg,
          icon:"error"
        })
      }
    })

  },
  // 完成
  cargoEnd(){
    this.setData({
      isShowCargo:true,
      checkFileNamw:""
    })
  },
  // 上传凭证
  uploadVideoZC(){
    let that = this;
    wx.chooseMedia({
      count:1,
      mediaType:["video","image"],
      sourceType: ['album', 'camera'],
      maxDuration: 30,
      success(res) {
        that.setData({
          selectType:res.type,
          CargoFileNameZC:res.tempFiles[0].tempFilePath,
          checkFileNamw:res.tempFiles[0].tempFilePath
        })
      }
    })
  },
    // 确定上传
  cargoSaveZC(){
    let that = this;
    if(this.data.CargoFileNameZC){
      request('/api/qiniu/uptoken', {},false,"GET").then((res) => {
        if(res.data.code == 200){
          this.setData({
            uptoken:res.data.data.uptoken
          })
          qiniuUploaderFun(this.data.CargoFileNameZC, this.data.selectType, this.data.uptoken).then((resp) => {
            let data = {
              id :that.data.orderId,
              chufei_thumb:resp.key,
            }
            that.saveFile(data)
          })
        }
      }) 
    }else{
      showToast({
        title:"请选择文件上传"
      })
    }
      
  },
  

  // 提交保存凭证
  saveFile(data) {
    let that = this
    request(`/api/chu-waste-order/${that.data.orderId }/complete`, data,true,"POST").then((res) => {
      if (res.data.code == 200) {
        showToast({
          title: "操作成功",
          icon: "success"
        })
        this.cargoCancel()
        setTimeout(() => {
          that.getDetails(that.data.orderId)
        }, 1500)
      } else {
        showToast({
          title: res.data.msg,
          icon:"error"
        })
      }
    })
   },

  // 取消完成
  cargoCancel(){
    this.setData({
      isShowCargo: false,
      CargoFileNameZC:null
    })
  },

  
    // 去运输方明细列表
  goYSlist(e) {
      let id = e.currentTarget.dataset.id
      wx.navigateTo({
        url: `../../../pages/chufei/yunshu/index?id=${id}`
      })
    },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      orderId: options.id,
      orderStatus : options.status
    })
    this.getDetails(options.id)
  },

  // 获取订单详情
  getDetails(id) {
    let data = {
      id :id 
    }
    request(`/api/chu-waste-order/${id}`, data,false,"GET").then((res) => {
      if (res.data.code == 200) {
        let driverInfo = res.data.data
        driverInfo.chan_waste.zhuangche_at = filtureDate(driverInfo.chan_waste.zhuangche_at)
        this.setData({
          driverInfo:driverInfo
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    wx.setNavigationBarColor({
      frontColor: '#ffffff',
      backgroundColor: '#0C89FB',
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getDetails(this.data.orderId)
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})