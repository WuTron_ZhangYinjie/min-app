import { request } from "../../../request/request"
import { showToast } from "../../../libs/asyncWx.js"
Page({
  data: {
    orderId: "",
    list: [],
  },
  // 去到运输方详情
  goysDetails(e) {
    let index = e.currentTarget.dataset.index
    wx.navigateTo({
      url: `../../../pages/chufei/ysDetails/ysDetails?id=${this.data.orderId}&index=${index}`
    })
  },
  onLoad: function (options) {
    this.setData({
      orderId:options.id
    })
    
  },
  getList(id) {
    request(`/api/chu-waste-order/${id}/data-list`, {id:id},false,"GET").then((res) => {
      if (res.data.code == 200) {
        let list = res.data.data
        this.setData({
          list:list
        })
      } else {
        showToast({
          title: res.data.msg,
          icon:"error"
        })
      }
    })
  },
  onReady: function () {

  },
  onShow: function () {
    this.getList(this.data.orderId)
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})