import { request,qiniuUploaderFun,filtureDate } from "../../../request/request"
import { showToast } from "../../../libs/asyncWx.js"
Page({

  data: {
    selectDate:"2022-03",
    isRuleTrue:false,
    isShowTrue:false,
    isShowCargo:false,
    chan_order_no:"",
    jie_statusList:[{name:"全部",id:0},{name:"待支付",id:1},{name:"已支付",id:2}],
    selectJie:0,
    orderStatusList:[
      {name:"全部",id:0},{name:"等待接单",id:1},{name:"通过",id:2},
      {name:"拒绝",id:3},
    ],
    selectOrder:0,
    jieshouStatusList:[
      {name:"全部",id:0},{name:"未接收",id:1},{name:"部分接收",id:2},
      {name:"已接收",id:3},
    ],
    selectJieShou:0,
    hasNext:false,
    page:1,
    list:[],
    chanfeiInfo: {},
    CargoFileNameZC: "",
    uptoken: "",
    reasonText: "",
    orderId:"",
    checkFileNamw:"",
  },

  // 获取订单号
  getNum(e){
    this.setData({
      chan_order_no:e.detail.value,
      page:1
    })
    this.getList()
  },
  
  bindDateChange(val){
    this.setData({
      selectDate:val.detail.value,
      page:1
    })
    this.getList()
  },
  // 打开筛选弹窗
  showModel(){
    this.setData({
      isRuleTrue:true
    })
  },
  // 关闭筛选弹窗
  closeToast(e) {
    this.setData({
        isRuleTrue: false,
    })
  },
  // 确定筛选
  cargoTrue(){
    this.getList()
    this.setData({
        isRuleTrue: false,
    })
  },

  // 选择结款状态
  changeJieStatus(e){
    this.setData({
      selectJie:e.currentTarget.dataset.id
    })
  },
  // 选择接收状态
  changeJieSStatus(e){
    this.setData({
      selectJieShou:e.currentTarget.dataset.id
    })
  },
  // 选择订单状态
  changeOrderStatus(e){
    this.setData({
      selectOrder:e.currentTarget.dataset.id
    })
  },
  // 去订单详情
  goDetails(e){
    let id = e.currentTarget.dataset.id
    let status = e.currentTarget.dataset.status
    wx.navigateTo({
      url: `../../../pages/chufei/details/details?id=${id}&status=${status}`
    })
  },
  // 回到个人中心
  goCenter(){
    wx.navigateBack({
      delta: 1
    })
  },
  // 同意接单
  arriveClick(e) {
    let that = this;
    this.setData({
      orderId:e.currentTarget.dataset.id
    })
    wx.showModal({
      title:"同意接单",
      content: '您确定同意接单吗？',
      success (resp) {
        if (resp.confirm) {
          let data = {
            id :that.data.orderId 
          }
          request(`/api/chu-waste-order/${that.data.orderId }/approve`, data,true,"POST").then((res) => {
            if (res.data.code == 200) {
              showToast({
                title: "操作成功",
                icon:"success"
              })
              that.setData({
                page:1
              })
              that.getList()
            } else {
              showToast({
                title: res.data.msg,
                icon:"error"
              })
            }
          })
        } else if (resp.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  // 拒绝接单
  arriveNotClick(e) {
    this.setData({
      isShowTrue: true,
      orderId:e.currentTarget.dataset.id
    })
  },
   // 获取拒绝内容
  getReason(e) {
    let text = e.detail.value
    this.setData({
      reasonText:text
    })
  },
  // 取消拒绝
  loadingCancel(){
    this.setData({
      isShowTrue:false
    })
  },
  // 提交拒绝
  loadingSubmit() {
    let that = this
    let data = {
      id: that.data.orderId,
      refuse_reason :this.data.reasonText
    }
    request(`/api/chu-waste-order/${that.data.orderId }/refuse`, data,true,"POST").then((res) => {
      if (res.data.code == 200) {
        showToast({
          title: "操作成功",
          icon:"success"
        })
        that.setData({
          page:1,
          isShowTrue:false
        })
        that.getList()
      } else {
        showToast({
          title: res.data.msg,
          icon:"error"
        })
      }
    })

  },
  // 完成
  cargoEnd(e){
    this.setData({
      isShowCargo: true,
      orderId:e.currentTarget.dataset.id,
      checkFileNamw:"",
    })
  },
  // 上传凭证
  uploadVideoZC(){
    let that = this;
    wx.chooseMedia({
      count:1,
      mediaType:["video","image"],
      sourceType: ['album', 'camera'],
      maxDuration: 30,
      success(res) {
        that.setData({
          selectType:res.type,
          CargoFileNameZC:res.tempFiles[0].tempFilePath,
          checkFileNamw:res.tempFiles[0].tempFilePath,
        })
      }
    })
  },
    // 确定上传
  cargoSaveZC() {
    let that = this
    if(this.data.CargoFileNameZC){
      request('/api/qiniu/uptoken', {},false,"GET").then((res) => {
        if(res.data.code == 200){
          this.setData({
            uptoken:res.data.data.uptoken
          })
          qiniuUploaderFun(this.data.CargoFileNameZC, this.data.selectType, this.data.uptoken).then((resp) => {
            let data = {
              id :that.data.orderId,
              chufei_thumb:resp.key,
            }
            that.saveFile(data)
          })
        }
      }) 
    }else{
      showToast({
        title:"请选择文件上传"
      })
    }
      
  },
  // 提交保存凭证
  saveFile(data) {
    let that = this
    request(`/api/chu-waste-order/${that.data.orderId }/complete`, data,true,"POST").then((res) => {
      if (res.data.code == 200) {
        showToast({
          title: "操作成功",
          icon:"success"
        })
        this.setData({
          page: 1,
          CargoFileNameZC:null,
          isShowCargo:false
        })
        setTimeout(() => {
          that.getList()
        },1500)
      } else {
        showToast({
          title: res.data.msg,
          icon:"error"
        })
      }
    })
  },
  
  // 取消完成
  cargoCancel(){
    this.setData({
      isShowCargo: false,
      CargoFileNameZC:null,
    })
  },
  onLoad: function (options) {
    let year  = new Date().getFullYear();
    let month = new Date().getMonth() + 1;
    if(month < 10){
       month = "0"+month
    }
    this.setData({
      selectDate:year+'-'+month
    })
    this.getList()
  },
  // 获取订单列表
  getList(isFirst = true){
    let data = {
      date_type :2,
      chu_order_no:this.data.chan_order_no,
      month:this.data.selectDate,
      jiekuan_status  :this.data.selectJie,
      chufei_status:this.data.selectJieShou,
      jiedan_status:this.data.selectOrder,
      page:this.data.page,
      limit:10
    }    
    request('/api/chu-waste-order', data, false, "GET").then((res) => {
      let chanfeiInfo = res.data.data
      for (let val of chanfeiInfo.list) {
        val.zhuangche_at = filtureDate(val.zhuangche_at )
       }
      this.setData({
        chanfeiInfo:chanfeiInfo
      })
      if(isFirst){  // 是否第一次加载
        if(chanfeiInfo.list.length == data.limit){
          this.setData({
            hasNext:true
          })
        }
        this.setData({
          list:chanfeiInfo.list
        })
      }else{
        if(chanfeiInfo.list.length < data.limit){
          this.setData({
            hasNext:false
          })
        }
        this.setData({
          list:[...this.data.list,...chanfeiInfo.list]
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.animation = wx.createAnimation();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if(this.data.hasNext){
      this.setData({
        page:this.data.page+1
      })
      this.getList(false)
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})