import { request,qiniuUploaderFun,filtureDate } from "../../../request/request"
import { showToast } from "../../../libs/asyncWx.js"
const qiniuUploader = require("../../../libs/qiniuUploader")
Page({
  data: {
    orderId: "",
    index:0,
    checkFileNamw:"",
    orderInfo: {},
    isShowCargo:false,
    CargoFileNameZC: "",
    uptoken: "",
    ysId:"",
  },
  // 去到路线
  lookLine(e) {
    let id = e.currentTarget.dataset.id
    wx.navigateTo({
      url: `../../../pages/chufei/map/map?id=${id}`
    })
  },
  // 查看凭证
  lookIMG(){
    let src = this.data.orderInfo.chu_jieshou_thumb
    wx.previewMedia({
      sources:[{url:src}],
      url:src
    })
  },
  onLoad: function (options) {
    this.setData({
      orderId: options.id,
      index:options.index,
    })
    this.getList(options.id,options.index)
  },
  getList(id,index) {
    request(`/api/chu-waste-order/${id}/data-list`, {id:id},false,"GET").then((res) => {
      if (res.data.code == 200) {
        let orderInfo = res.data.data[index]
        orderInfo.transport_at = filtureDate(orderInfo.transport_at)
        orderInfo.finish_at = filtureDate(orderInfo.finish_at)
        this.setData({
          orderInfo:orderInfo
        })
      } else {
        showToast({
          title: res.data.msg,
          icon:"error"
        })
      }
    })
  },
  // 上传凭证
  cargoEnd(e){
    this.setData({
      isShowCargo: true,
      checkFileNamw:"",
      ysId:e.currentTarget.dataset.id
    })
  },
  // 上传凭证
  uploadVideoZC(){
    let that = this;
    wx.chooseMedia({
      count:1,
      mediaType:["video","image"],
      sourceType: ['album', 'camera'],
      maxDuration: 30,
      success(res) {
        that.setData({
          selectType:res.type,
          CargoFileNameZC:res.tempFiles[0].tempFilePath,
          checkFileNamw:res.tempFiles[0].tempFilePath,
        })
      }
    })
  },
    // 确定上传
  cargoSaveZC() {
    let that = this
    if(this.data.CargoFileNameZC){
      request('/api/qiniu/uptoken', {},false,"GET").then((res) => {
        if(res.data.code == 200){
          this.setData({
            uptoken:res.data.data.uptoken
          })
          qiniuUploaderFun(this.data.CargoFileNameZC, this.data.selectType, this.data.uptoken).then((resp) => {
            let data = {
              transport_order_id :that.data.ysId,
              chu_jieshou_thumb :resp.key,
            }
            that.saveFile(data)
          })
        }
      }) 
    }else{
      showToast({
        title:"请选择文件上传"
      })
    }
      
  },

  // 提交保存凭证
  saveFile(data) {
    let that = this
    request(`/api/chu-waste-order/${that.data.ysId }/chu_jieshou_thumb`, data,true,"POST").then((res) => {
      if (res.data.code == 200) {
        showToast({
          title: "操作成功",
          icon:"success"
        })
        this.setData({
          CargoFileNameZC:null,
          isShowCargo:false
        })
        setTimeout(() => {
          that.getList(that.data.orderId,that.data.index)
        },1500)
      } else {
        showToast({
          title: res.data.msg,
          icon:"error"
        })
      }
    })
  },
  
  // 取消完成
  cargoCancel(){
    this.setData({
      isShowCargo: false,
      CargoFileNameZC:null,
    })
  },
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})