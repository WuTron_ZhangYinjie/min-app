// pages/login/index.js
import { request } from "../../request/request.js" 
import { showToast } from "../../libs/asyncWx.js"
Page({
  /**
   * 页面的初始数据
   */
  data: {
    array:[
      {name:"司机",id:1},
      {name:"产废方",id:2},
      {name:"处废方",id:3}
    ],
    loginForm:{
      type:"",
      typeName:"",
      account:"",
      password:"",
    },
  },
  // 切换类型
  bindPickerChange(val){
    let loginForm = this.data.loginForm
    loginForm["type"] = val.detail.value
    loginForm["typeName"] = this.data.array[val.detail.value].name
    this.setData({
      loginForm :loginForm
    })
  },
  // 获取账号
  getAccount(val){
    let loginForm = this.data.loginForm
    loginForm["account"] = val.detail.value
    this.setData({
      loginForm :loginForm
    })
  },
  // 获取密码
  getPassword(val){
    let loginForm = this.data.loginForm
    loginForm["password"] = val.detail.value
    this.setData({
      loginForm :loginForm
    })
  }, 
  // 登录
  loginAction(){
    let that = this;
    let flag = false

    if(this.data.loginForm.type == 0){
      flag = true
      wx.getLocation({
        type: 'wgs84',
        success (res) {
          const latitude = res.latitude
          const longitude = res.longitude
          const speed = res.speed
          const accuracy = res.accuracy
        }
       })
    }else{
      flag = true
    }
    if(flag){
      let prams = {
        type :Number(that.data.loginForm.type) + 1,
        account :that.data.loginForm["account"],
        password :that.data.loginForm["password"],
      }
      request('/api/login', prams).then((resp) => {
        if(resp.data.code == 200){
          wx.setStorageSync('token', resp.data.data.token)
          wx.setStorageSync('userInfo', JSON.stringify(resp.data.data.userInfo))
          wx.setStorageSync('loginTppe', this.data.loginForm.type) 
          wx.reLaunch({
            url: '../../pages/center/index'
          })            
        }else{
          showToast({ title: resp.data.msg, icon: "error" })
        }
      })
    }        
      
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // wx.clearStorage() userInfo
    wx.removeStorageSync("token");
    wx.removeStorageSync("userInfo");
    wx.removeStorageSync("loginTppe");
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})