// pages/password/index.js
import { request } from "../../request/request"
import { showToast } from "../../libs/asyncWx.js"
Page({
  data: {
    oldPass:"",
    newPass:"",
    aginPass:""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },
  // 获取旧密码
  getOldPass(e){
    this.setData({
      oldPass:e.detail.value
    })
  },
  // 新密码
  getNewPass(e){
    this.setData({
      newPass:e.detail.value
    })
  },
  // 确认密码
  getAginPass(e){
    this.setData({
      aginPass:e.detail.value
    })
  },

  // 修改密码确定  /api​/member​/modify-password修改密码(适用于:产废方/处废方/司机端)
  changePass(){
    let data = {
      old_password:this.data.oldPass,
      new_password:this.data.newPass,
      confirm_password :this.data.aginPass
    }
    request('/api/member/modify-password', data,true,"POST").then((res) => {
      if(res.data.code == 200){
        wx.navigateTo({
          url:"../../pages/login/index"
        })
      }else{
        showToast({ title: res.data.msg, icon: "error" })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})