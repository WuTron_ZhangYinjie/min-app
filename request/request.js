var app = getApp()
const qqMap = require('../libs/qqmap-wx-jssdk')
const QQMapWX = new qqMap({
  key: app.globalData.key // 必填
});
const qiniuUploader = require("../libs/qiniuUploader")
//同时发送异步代码的次数
let ajaxTimes = 0;
//使用promise封装一个 请求
export function request(url,data,isParams=false ,method = "POST",){
    ajaxTimes ++  //发送一次请求就加一次
    // 在请求 数据回来之前 显示一个加载中
    wx.showLoading({
        title: '加载中',
    })
    //定义公共部分 url 
  const baseUrl = app.globalData.xhrUrl
    //将传过来的参数结构出来
    // console.log(isParams)
    if(method == "POST" && isParams){
        return new Promise((resolve,reject)=>{
            // 发起请求  
            const keys = Object.keys(data)
            let params = '?'
            for (let i = 0; i < keys.length; i++) {
                const key = keys[i]
                let value = data[key]
                if (value != null && value != '') {
                    if (i != 0) {
                        params += '&'
                    }                    
                    value = encodeURIComponent(value)//对特殊字符进行转义
                    params += key + '=' + value
                }

            }        
            wx.request({
                url:baseUrl + url +params,
                header: {
                    'content-type': 'application/json', // 默认值
                    'token': wx.getStorageSync('token'),    
                },
                method:method,
                success:(res)=>{
                    //成功的回调
                    if(res.data.code == 100){
                        wx.navigateTo({
                            url: '/pages/login/index'
                        })
                    }else{
                        resolve(res)
                    }                    
                },
                fail:(err)=>{
                    //失败的回调
                    reject(err)
                },
                complete:()=>{
                    ajaxTimes -- //请求完成一次 ，就减一次
                    if(ajaxTimes===0){
                        //当所有的请求都完成时，就关闭 加载中 弹框
                        wx.hideLoading()
                    }
                }
            });
        });
    }else{
        return new Promise((resolve,reject)=>{
            // 发起请求          
            wx.request({
                url:baseUrl + url,
                data:data,
                header: {
                    'content-type': 'application/json', // 默认值
                    'token': wx.getStorageSync('token'),    
                },
                method:method,
                success:(res)=>{
                    //成功的回调
                    if(res.data.code == 100){
                        wx.navigateTo({
                            url: '/pages/login/index'
                        })
                     }else{
                         resolve(res)
                     }
                },
                fail:(err)=>{
                    //失败的回调
                    reject(err)
                },
                complete:()=>{
                    ajaxTimes -- //请求完成一次 ，就减一次
                    if(ajaxTimes===0){
                        //当所有的请求都完成时，就关闭 加载中 弹框
                        wx.hideLoading()
                    }
                }
            });
        });
    }
    
}

export function qiniuUploaderFun(fileName, selectType,uptoken) {
    let token =  wx.getStorageSync('token')
    var pathX = fileName + ''
    var qiniu_key = ""
    if(selectType == "image"){
      qiniu_key = Date.parse(new Date()) / 1000 + ".jpg"
    }else if(selectType == "video"){
      qiniu_key = Date.parse(new Date()) / 1000 + ".mp4"
    }
    return new Promise((resolve,reject) => {
            qiniuUploader.upload(pathX, (result) => {
            // 将图片url上传给服务器
                resolve(result)        
            }, (error) => {

            }, {
            key: qiniu_key,
              region: 'ECN', // 华南区
              uptoken:uptoken,            // 从自己的服务器获取
              uploadURL: 'https://cf.54past.com/api/qiniu/uptoken?token=' + token,      // 从自己的服务器获取
              domain: 'http://chufei-img.54past.com'         // 从自己的服务器获取
            });
        })
}

export function $direction(from, to, type = 1) {
    let modeArr = ['driving', 'walking', 'bicycling', 'transit'];
    return new Promise((resolve, reject) => {
      QQMapWX.direction({
        mode: modeArr[type - 1],
        from,
        to,
        success(res) {
          let result = res.result.routes[0]
          resolve(result)
        },
        fail(error) {
          console.log(error)
        }
      })
    })
}
  
export function filtureDate(date) {
    if(date && date!=null){
        if(date.slice(0,10)){
            return date.slice(0,10)
        }else{
            return "--"
        }
         
        // var currentDate = new Date(date);
        // if(currentDate){
        //     var year =   currentDate.getFullYear();
        //     var month = currentDate.getMonth() + 1;
        //     var day = currentDate.getDate();
        //     if (month < 10) {
        //     month = "0" + month;
        //     }
        //     if (day < 10) {
        //     day = "0" + day;
        //     }
        //     return year + "-" + month + "-" + day;
        // }else{
            
        // }
        
    }else {
        return "--"
    }
  
}